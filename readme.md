# POC quick styleguide with Jekyll

## Quick install

Install Jekyll and Bundler gems through RubyGems

`gem install jekyll bundler`

In the directory, build the site on the preview server

`bundle exec jekyll serve`

Now browse to http://localhost:4000

See the [Jekyll documentation](https://jekyllrb.com/docs/home/) for more infos.

## Tutorial

### 1. Install jekyll

`gem install jekyll bundler` 

### 2. Create new project

`jekyll new poc-styleguide`

![](img-readme/C9E2FF44-5BF7-441B-AD39-9CBF40192016.png)


### 3. Enter the new project

`cd poc-styleguide`

Here is what we have:

![](img-readme/C4197814-3545-4CC0-B45F-BC9BBD2756EC.png)


### 4. Clean and structure the project

(see [https://github.com/HugoGiraudel/jekyll-boilerplate](https://github.com/HugoGiraudel/jekyll-boilerplate))

#### Initialize configuration in `_config.yml`

- Build settings

![](img-readme/928CD483-B0FE-4095-B325-1938D76879E1.png)

- Kramdown (jekyll' syntax highlighter)

![](img-readme/72CCB7FA-CF12-4EE6-B292-28AE3103781C.png)

- Sass compilation and minification

![](img-readme/2CA33EB6-49F7-479A-8C31-B18688A20147.png)

- Exclude some files from the build conversion

![](img-readme/4928D95A-0531-43FF-A1BE-52E825B4644F.png)


#### Deletions

- Remove the samples: `_posts` folder, markdown files, ...


#### Additions
- add build, mac crap and node_modules in .gitignore
- add all the sources in a global `src` folder
- add `_includes` folder with basic `head.html` component

![](img-readme/C03BD58F-E5C9-4D51-9E50-9128730A5131.png)

- add `_layouts` folder with basic `default.html` layout

![](img-readme/6FBAD22C-976C-4B3C-AB6A-6B9A76FE8C76.png)

- add basic `index.html` view

![](img-readme/30F42724-DA99-4778-8392-6ED5BDD20A8D.png)

- add css, js and images assets folders and basic styles

![](img-readme/CE437F7D-E681-4FB1-A77A-F97336664C46.png)

For testing purpose, we add a `_shame.scss` with:

![](img-readme/10283E94-FC01-4D76-9FD2-AF0FA2B3C951.png)

So the folder looks like this now:

![](img-readme/78772D45-9CE7-42E2-9047-771BA321E44D.png)

Let’s test the updated poc:
`bundle exec jekyll serve`

![](img-readme/1C81DED1-E85F-4C13-9F8B-51B1A55105B6.png)

It generates our `build` folder

![](img-readme/ABF2BDC8-652D-4F6D-AC7D-20C8E99D6BB8.png)

The poc is available on localhost (port 4000 in this case):

![](img-readme/4DE71262-0B09-44CA-9F30-F43E755BA8F8.png)

### 5. Save the project

On gitlab for instance:

![](img-readme/3C670AEE-F904-425B-AB24-988D7A3EEE0B.png)

![](img-readme/A9592ADE-5523-49B3-A771-05CB548027A4.png)

![](img-readme/6EF98815-0677-4847-895F-083406D8EECD.png)

### 6. Configure the collection of components

Create a collection named « components » in the config

![](img-readme/278F0A42-82D8-4D49-BD83-15AFB0886CDF.png)

We set the output to true so we could have a file for each component.
This tells Jekyll that we will have entries from our components collection in a `_components` folder at project’s root level.
We create a new layout extending the first one, to display our components:

![](img-readme/63D3C8D3-0304-428D-98C0-969F4E87AC49.png)

We create for instance a button component, using this new layout:

![](img-readme/CC950889-8CF8-4D3D-AAFB-8F91F4B85048.png)


![](img-readme/7380B100-1174-4790-9921-44CBBBC77DC3.png)

We have our first component’s page:

![](img-readme/A15914F3-5D1B-4AF6-85C5-7DD0F4D8B2E7.png)

Commit and push on git

### 7. Refine the styleguide

- Create multiple component samples

![](img-readme/86705257-9958-4018-B6FD-39BFFBA9876C.png)

It also works with the markdown extension:

![](img-readme/0771804B-337B-408A-9CA6-2F6F88048E20.png)

- Add more infos on each component in their front matter

![](img-readme/A9BB1A18-2E81-49B6-ABBD-305EF230D7FC.png)

A specific title.
A type to regroup components together.
Some sources for the styles.
A description.

- Display the infos in the layout:

![](img-readme/65A07B80-FC6B-42ED-8CD0-AA5A24E1ACE3.png)

- Eventually add some styles to the components

![](img-readme/CF89DF67-53ED-4B30-9F8E-CD05767E5722.png)

Commit and push on git

### 8. Display a navigation

We edit the default layout to display the navigation through our components, grouped by type

![](img-readme/5C45DAC8-1ADD-47A8-BBA2-013EB20AE2AD.png)

And we get our navigation:

![](img-readme/00476558-CB19-4AE2-AF86-ADC749CC1D1F.png)

Commit and push on git

### 9. Bootstrap styles.

Load Bootstrap and the css styleguide specific styles we want: 

![](img-readme/062DA5AD-EB32-424C-9BC2-77B851C486EF.png)

(see [https://codepen.io/tibomahe/pen/qPJQwQ](https://codepen.io/tibomahe/pen/qPJQwQ))

Add in a footer the interactivity we will need for the navigation:

![](img-readme/316CA730-0E04-496E-AFF7-70C0A6E02D41.png)

Update the markup (see [https://codepen.io/tibomahe/pen/qPJQwQ](https://codepen.io/tibomahe/pen/qPJQwQ))

Choose style for the code preview: [https://github.com/jwarby/jekyll-pygments-themes](https://github.com/jwarby/jekyll-pygments-themes) and add it with the other styles (for instance we chose `trac.scss` here):

![](img-readme/B153A396-B681-44F2-86F5-B5DDFE767B40.png)

Commit and push on git

### 10. Add some introduction to the styleguide

Update the index.html/md

![](img-readme/2113A309-1FC7-4681-8BB4-773C738121B7.png)

We can also add a section with various resources and context around the styleguide. We could create a new collection for these overview pages:

![](img-readme/6C353416-4ED2-4692-ABDD-405E23C84A6D.png)

Create the corresponding files and folder:

![](img-readme/42626B1E-F2A3-46A6-BCE9-FEE072280136.png)

Writing the markdown views:

![](img-readme/57B91102-9EE3-4DA3-8273-633C162CE735.png)

### 11. (BONUS) More complex styleguide

We could have the need to insert more than a sample in a view, for instance showing different buttons, lists, etc.

Liquid (behind Jekyll) is limited, it is doable though, but not without some struggling.

The first limitation we have is that our component template is made to only show one component, the one shown in the `{{ content }}` section: 

![](img-readme/651C035B-191B-4D5F-B5B0-DA101D714754.png)

We can no longer use this section since there is no mean to distinguish the different components we could craft in the section.
The idea then is to translate the code section to the front matter. This way, creating a new component is just like filling a form with different inputs and entries:

![](img-readme/79969069-0AE8-4080-9A3B-E973DFCFDC1D.png)

We add some other inputs so we can add some infos to our component.

Note that we add a « 1 » to indicate that it is the first element of our page.

in the same way we can add a second component:

![](img-readme/3D04815E-A708-44C7-9E9A-5870CA33E17B.png)

Since we are no longer using the `{{ content }}` global variable, we need to update our component template to tell it to:

1) Show the component regarding the local page variables we just created ;

2) Tell it to loop if there is several components

Here we then are displaying the variable of our component (in the component template):

![](img-readme/E433AE88-4225-45E7-980A-CDD7F59125BA.png)

And then we loop over this code with a `for` iteration:

![](img-readme/4FF76F30-7ED5-4B86-9957-6522478AC123.png)

Note that one difficulty is that the page variables are designed in yaml, but the templating is made with Liquid. Jekyll manages to process and connect both languages. But here Liquid doesn’t know how many components are defined in the jekyll yaml front matter, so the trick is that we have to tell it the number of item in a `page.sample.length` variable so the loop can know when to stop:

![](img-readme/8ECD42E9-A3FF-44DD-82E7-1AAAFFD36B45.png)

And here we go:

![](img-readme/3CBBB9BE-EBDD-4748-AD71-F3B590B5B44F.png)

