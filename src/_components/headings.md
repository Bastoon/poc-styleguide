---
layout: component
title: "Headings"
type: typography
scss_name: "_headings.scss"
scss_path:
description: "Since text is the primary and most essential way to convey informations and values to the users, it's important to grant typography a primordial attention. This section stresses the typographic guidelines that should be used on all websites."
sample:
    length: 1
    1:
        title:
        description:
        code: |
            <h1>Heading 1 title</h1>
            <h2>Heading 2 title</h2>
            <h3>Heading 3 title</h3>
            <h4>Heading 4 title</h4>
            <h5>Heading 5 title</h5>
---

