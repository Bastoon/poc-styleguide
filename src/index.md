---
layout: default
---


<h1 class="sg-h1">Introduction</h1>
<p class="sg-lead">Welcome to our styleguide !</p>

It provide base styles and all the HTML (Twig) / CSS (SCSS) components used in our website.
A styleguide fosters and offers many advantages :

<ul class="sg-ul">
	<li>consistency and logic in visual identity, leading to a better UX;</li>
	<li>fast and easy component testing and prototyping;</li>
	<li>better and more methodical workflow with easier modular components design and faster view assembly;</li>
	<li>extensible and scalable as much as you want, for improved functionalities or new views;</li>
	<li>common system vocabulary and basis definition, facilitating communication to all the actors, team members as well as clients;</li>
	<li>facilitate the arrival of new designers and developpers in the project.</li>
</ul>

This styleguide is mobile responsive and has cross-browser support (we no longer support IE 9 or lower), built on top of [Bootsrap Sass](https://github.com/twbs/bootstrap-sass){:target="_blank"}, the official port of [Bootstrap 3](https://getbootstrap.com/){:target="_blank"}.
